VIDEO_URL := "https://tile.loc.gov/storage-services/service/mbrs/ntscrm/00068306/00068306.mp4"

VIDEOS := $(addprefix assets/,popeye-small.mov popeye-medium.mov popeye-large.mov)

all: $(VIDEOS)

directory-setup:
	mkdir $(ASSET_DIR)


assets/%.mov: assets/%.mp4
	ffmpeg -i $< -c copy -f mov $@

assets/popeye-small.mp4:
	wget -O $@ $(VIDEO_URL)

assets/popeye-medium.mp4: assets/popeye-small.mp4
	yes "file $<" | head -n 5 > inputs-medium.txt
	ffmpeg -f concat -i inputs-medium.txt  -c copy $@
	rm inputs-medium.txt

assets/popeye-large.mp4: assets/popeye-small.mp4
	yes "file $<" | head -n 10 > inputs-large.txt
	ffmpeg -f concat -i inputs-large.txt  -c copy $@
	rm inputs-large.txt


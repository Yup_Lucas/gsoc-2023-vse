## Asset set up

In order to profile Blender and determine if there were speed improvements
we'll use a fixed set of video assets.

The most common video file formats used by video editors are MOV and MP4.

In the `scripts` folder there's a file called `Assets.make` that can be used to download and
set up all of the assets we'll used to take measurements.

**NOTE: You must be connected to a US-based VPN in order to download the video from the Library of Congress.**

### Dependencies

You'll need to have `ffmpeg` and `wget` installed in order to use the Makefile provided.

If you are on an Apple device, you can used Homebrew to install the dependencies:

```
brew install ffmpeg wget
```


### Running the script

From the root of the project directory, run the commands below:

```
mkdir assets
make -f scripts/Assets.make
```

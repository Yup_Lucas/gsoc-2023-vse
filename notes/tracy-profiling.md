
## Context
[tracy](https://github.com/wolfpld/tracy) is an open-source C++ profiler. While it can be used as a sampling profiler, it also works as a frame profiler.

Tracy allows you to annotate code sections (zones) and collect metrics about that specific section. It  can make it easier to visualize just the care about in a profiling session.

## Download the Tracy GUI client

You can download the client from the releases page: https://github.com/wolfpld/tracy/releases

In my set up I used version v0.9.1

The client is used to connect to the instrumented application (Blender) you want to profile and collect the metrics for visualization.

Inside the `Tracy-0.9.1.7z` archive there's a `Tracy.exe` file. That's the GUI client.

## Add tracy as a dependency

### Git submodule set up

1. Go to `blender-git/blender/extern`
2.  Add tracy as a submodule with `git add submodule https://github.com/wolfpld/tracy/ tracy`
3. Checkout the version that matches the client you are using: `cd tracy && git checkout v0.9.1` 

**NOTE:** Make sure the library version matches the GUI client. If those differ, the wire protocol used will not match and the GUI client will be unable to read the messages.

### CMake config changes

### Add tracy as an extern subdirectory

In `blender-git/blender/extern/CMakeLists.txt`, apply the patch below:
```diff
@@ -96,4 +96,8 @@ if(WITH_VULKAN_BACKEND)
   add_subdirectory(vulkan_memory_allocator)
 endif()

-add_subdirectory(tracy)
+if(WITH_TRACY)
+  option(TRACY_ENABLE "" ON)
+  option(TRACY_ON_DEMAND "" ON)
+  add_subdirectory(tracy)
+endif()
```

I added the `WITH_TRACY` option to be able to easily turn it on and off.

### Enable Tracy for the entire project

Apply the patch below to `blender-git/blender/CMakeLists.txt`:
```diff
@@ -674,6 +675,8 @@ endif()
 option(WITH_COMPILER_ASAN "Build and link against address sanitizer (only for Debug & RelWithDebInfo targets)." OFF)
 mark_as_advanced(WITH_COMPILER_ASAN)

+option(WITH_TRACY "Build and link against Tracy (used for profiling in RelWithDebuInfo and Release targets)." ON)
+
 if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_C_COMPILER_ID MATCHES "Clang")
   if(WITH_COMPILER_ASAN)
     set(_asan_defaults "\
@@ -1807,6 +1810,7 @@ endif()

 if(WITH_BLENDER)
   add_subdirectory(intern)
+  add_definitions(-DTRACY_ENABLE)
   add_subdirectory(extern)

   # source after intern and extern to gather all
@@ -2005,6 +2009,9 @@ if(FIRST_RUN)
     endif()
   endif()

+  info_cfg_text("Profiling:")
+  info_cfg_option(WITH_TRACY)
+
   info_cfg_text("")

   message("${_config_msg}")
```

-----------------------------------------------------------

With those patches applied Blender will be built with Tracy embedded in it.

## Example usage

I wanted to measure how long computing an audio strip waveform would take in the sequence editor (`blender-git/blender/source/blender/editors/space_sequencer`).

I applied the following changes to the `sequencer_preview.c` file:

```diff
@@ -23,6 +23,7 @@
 #include "MEM_guardedalloc.h"

 #include "sequencer_intern.h"
+#include "../../../extern/tracy/public/tracy/TracyC.h"

 typedef struct PreviewJob {
   ListBase previews;
@@ -53,6 +54,9 @@ static void free_preview_job(void *data)
 /* Only this runs inside thread. */
 static void preview_startjob(void *data, bool *stop, bool *do_update, float *progress)
 {
+
+  TracyCZoneN(preview_startjob_ctx, "preview_startjob", true);
+
   PreviewJob *pj = data;
   PreviewJobAudio *previewjb;

@@ -64,7 +68,9 @@ static void preview_startjob(void *data, bool *stop, bool *do_update, float *pro
     PreviewJobAudio *preview_next;
     bSound *sound = previewjb->sound;

+    TracyCZoneN(sound_read_waveform_ctx, "sound_read_waveform", true);
     BKE_sound_read_waveform(previewjb->bmain, sound, stop);
+    TracyCZoneEnd(sound_read_waveform_ctx);

     if (*stop || G.is_break) {
       BLI_mutex_lock(pj->mutex);
@@ -100,6 +106,7 @@ static void preview_startjob(void *data, bool *stop, bool *do_update, float *pro
     *do_update = true;
     BLI_mutex_unlock(pj->mutex);
   }
+  TracyCZoneEnd(preview_startjob_ctx);
 }

 static void preview_endjob(void *data)
```
Note that the include directive at the top of the file is for the `TracyC.h` header. We can't use the regular C++ headers here since the sequencer is written in C.

After that, I changed Blender's CMake config to build a release version but with **debug** information. There's no point in profiling a debug build since that's not what we'll ship to people anyways. I edited `blender-git/blender/CMakeLists.txt` like so:
```diff
@@ -60,6 +60,7 @@ set_property(DIRECTORY APPEND PROPERTY COMPILE_DEFINITIONS
   $<$<CONFIG:RelWithDebInfo>:NDEBUG>
 )

+set(CMAKE_BUILD_TYPE RelWithDebInfo)

```
With these changes in place, I just built tracy with `make debug ninja`

### Running the profiler

#### Setting up the profiling session

1. Launch blender: `build_windows_x64_vc17_Debug\bin\blender.exe`
2. In Blender, change to the video sequencer view.
3. Drag-and-drop a video file to the Blender window.
4. Launch the Tracy.exe GUI client. It'll take a few seconds but it should show you the Blender process a client it can connect to. Select it and hit `Connect`.

#### Actually profiling

1. In the Tracy Profiler GUI, hit `Start` to begin collecting metrics.
2. Move over to Blender and toggle the option to display the audio track's waveform:
	1. Select the audio track.
	2. Press the `N` key.
	3. Check the `Display waveform` checkbox.
3. Wait till the waveform appears in the UI, go back to the Tracy Profiler GUI and stop the session

--------------------------------------------------------

Once that is done, you can click the `Statistics` button in the Tracy Profiler GUI and see the breakdown of time spent:

![stats](../images/tracy-stats.png)

For the video I used as a test case, we can see most of the time was spent inside the `sound_read_waveform` function.

## Known issues

When building blender with the MSVC toolchain (Visual Studio 2022 v17.4.1), the build process fails due to the static initialization of a struct.
In C, a struct can be statically initialized only if all of the initalizer values are constants.

TracyC.h makes deliberate use of static struct inline initialization, but for some reason MSVC is not happy with that.
All of the values used in the initialization are constants (from C's PoV).

I ended up having to build the project with Ninja and things worked fine when I did that:

```
make debug ninja
```
